#ifndef ABSTRACT_ENGINE_H
#define ABSTRACT_ENGINE_H

// Employ Singleton

// Instructions:
//	Friend this class so it can access the constructor of the derived class
//	Privitize constructor of derived class
//	Privitize copy constructor of derived class
//	Privitize assignment operator of derived class

template <typename T>
class AbstractEngine
{
public:
	// Don't free the pointer returned by this.
	static T* getInstance();
	~AbstractEngine();

private:
	static T* globalInstance;

	AbstractEngine(const AbstractEngine&);
	AbstractEngine& operator=(const AbstractEngine&);

protected:
	// Make protected so derived classes can properly use it in their constructors.
	AbstractEngine() {}

};

#include "AbstractEngine.hpp"

#endif