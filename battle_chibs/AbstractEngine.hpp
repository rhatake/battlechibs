#ifndef ABSTRACT_ENGINE_HPP
#define ABSTRACT_ENGINE_HPP

#include "AbstractEngine.h"

template <typename T> T* AbstractEngine<T>::globalInstance = 0;

template <typename T>
T* AbstractEngine<T>::getInstance()
{
	if (globalInstance) {
		return globalInstance;
	}
	else {
		globalInstance = new T();
		return globalInstance;
	}
}

template <typename T>
AbstractEngine<T>::~AbstractEngine()
{
	delete globalInstance;
}

#endif