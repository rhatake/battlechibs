#include "InputEngine.h"
#include "AbstractEngine.h"
#include "GameException.h"

#include "SDL.h"

#include <iostream>

// In order to gather input we have to have access the the event queqe,
// we only get access the that if video is initialized and the video mode is
// set.

InputEngine::InputEngine()
	: AbstractEngine()
{
	// I'm using "status" for return values because I've run into optimization errors before
	// when just putting the function call with and if statement like so: if (call())
	int status;

	status = SDL_WasInit(SDL_INIT_VIDEO);

	// If the subsystem is off
	if (status == 0) {
		throw GameException(GameException::Type::SDL_UPSTREAM_SYSTEM, "Video Subsystem Not Initialized");
	}

	status = (int) SDL_GetVideoSurface();
	// If the video mode hasn't been set
	if (status == 0) {
		throw GameException(GameException::Type::SDL_UPSTREAM_SYSTEM, "Video Mode Not Set");
	}

	// This should replay keypress events. I'm setting the delays to 1 in both cases.
	// One for responsivness, and the other because it could fundamentally alter the
	// speed of the game. Note that setting first argument to 0 would actually disable
	// key repeating.
	status = SDL_EnableKeyRepeat(1, 1);
	
	// However, this not working isn't necesarrily gamebreaking. so we'll just
	// warn about it if it fails.
	if (status == -1)
		std::cerr << "Warning SDL_EnableKeyRepeat Failed: " << SDL_GetError() << "\n";

	 SDL_EnableUNICODE(1);
}

char InputEngine::getInput(void)
{
	// Use SDL for this

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_KEYDOWN:
				return *SDL_GetKeyName(event.key.keysym.sym);

			default:  // This case shouldn't come up
				return 0;
		}
	}

	return 0;
}

void InputEngine::testUnit(void)
{
	int status;
	SDL_Surface* screen;
	InputEngine* instanceOne;

	std::cout << "\nTesting: InputEngine::getInput()" << std::endl;
	std::cout << "\tSetting up Screen" << std::endl;
	std::cout << "\t\tInitializing Video: ";

	status = SDL_InitSubSystem(SDL_INIT_VIDEO);

	if (status == -1)
		throw GameException(GameException::Type::SDL_FAIL_INIT, SDL_GetError());

	std::cout << "Success" << std::endl;

	std::cout << "\t\tSetting video mode: ";

	screen = SDL_SetVideoMode( 320, 200, 0, 0 );

	if (screen == 0)
		throw GameException(GameException::Type::SDL_FAIL_INIT, SDL_GetError());

	std::cout << "Success\n" << std::endl;

	instanceOne = InputEngine::getInstance();

	std::cout << "Press any key to test InputEngine::getInput\n'c' to continue test" << std::endl;

	char character = ' ';
	while (character != 'c') {
		while (!(character = instanceOne->getInput()))
			SDL_Delay(33);

		std::cout << "\tKey pressed: " << character << "\n";
	}

	// Next Test ///////////////////////////////////////////////////////////////////////

	std::cout << "\nTesting: Singleton Interface" << std::endl;

	const InputEngine* instanceTwo;

	std::cout << "\tCreating Second instance of InputEngine" << std::endl;

	instanceTwo = InputEngine::getInstance();

	std::cout << "\tAddress of first instance:\t" << instanceOne << std::endl;
	std::cout << "\tAddress of second instance:\t" << instanceTwo << std::endl;

	if (instanceOne == instanceTwo)
		std::cout << "\tSingleton Successful.\n" << std::endl;
	else
		std::cerr << "\tSingleton Unseccessful.\n" << std:: endl;


	// Note, the subsytems I have started here are not shutdown again.
}