#ifndef FPSREGULATOR_H
#define FPSREGULATOR_H

#include "Timer.h"

class FPSRegulator
{
public:
	// assumes milliseconds
	FPSRegulator(double desiredFrameRate = 30.0, double permitted_error = 5.0);

	// recalculate fps
	double FPSUpdate(void);
	double getFPS(bool recalc = false);
	void stabilize(int timeout = 10000);
	bool stable(void);

private:
	double startTime;
	double frameCount;
	double lastFPS;

	int step;
	int increment;

	const double error;

	Timer timer;

	const double secondUnits;
	const double desired;

	void reset(void);
	bool anyOverflows(void);
};

#endif