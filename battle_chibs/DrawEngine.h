#ifndef DRAWENGINE_H
#define DRAWENGINE_H

#include "AbstractEngine.h"
#include "Vector.h"

#include <map>

#include "SDL.h"
#include "SDL_image.h"

class DrawEngine : public AbstractEngine<DrawEngine>
{
public:
	~DrawEngine();

	typedef SDL_Rect Rect;

	struct RGB {
		Uint8 red;
		Uint8 green;
		Uint8 blue;
	};

	int getScreenWidth(void);
	int getScreenHeight(void);

	void updateDisplay(void);

	int createImageEntry(const char* file_name);
	void deleteImageEntry(int entry_ID);

	void drawEntry(int entry_ID, Rect* srect = 0, int where_x = 0, int where_y = 0);

	void drawPixel(const RGB*, const Vector*, bool update_screen = true);
	void drawPixel(int red, int green, int blue, int xpos, int ypos, bool update_screen = true);

	void drawLine(const RGB*, const Vector*, const Vector*, bool update_screen = true);
	void drawLine(int red, int green, int blue, int from_x, int from_y, int to_x, int to_y, bool update_screen = true);

	void fillRect(const RGB* color, const Rect* area, bool update_screen = true);
	void fillscreen(const RGB* color, bool update_screen = true);

private:
	// AbstractEngine setup
	friend AbstractEngine;
	DrawEngine(int width = 640, int height = 480, int bpp = 8);
	DrawEngine(const DrawEngine&);
	DrawEngine& operator=(const DrawEngine&);

	std::map<int, SDL_Surface*> imageBucket;
	// screen shouldn't be freed by caller
	SDL_Surface* screen;

	int index;
};

#endif