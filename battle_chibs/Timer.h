#ifndef TIMER_H
#define TIMER_H

class Timer
{
public:
	Timer();
	~Timer();

	// Give some time back to the OS for a number of milliseconds
	void sleep(int time);

	// Get some time in milliseconds
	int getTimeMilliseconds(void);
};

#endif