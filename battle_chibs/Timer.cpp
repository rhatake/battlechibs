#include "Timer.h"

#include "SDL.h"

Timer::Timer()
{

}

Timer::~Timer()
{

}

void Timer::sleep(int time)
{
	SDL_Delay(time);
}

int Timer::getTimeMilliseconds(void)
{
	return SDL_GetTicks();
}