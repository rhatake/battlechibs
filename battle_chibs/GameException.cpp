#include "GameException.h"

const char *GameException::Description[] = {
	"Generic exception occured",	// Placeholder Exception.
	"SDL Failed to Initialize",
	"A Required Subsystem was not Enabled",
	"File Not Found",
	"An Internal Error Occured",
	"Failed to Lock Surface",
	"Tried to Write to Memory Outside Zero'd Display",

	// Add more exception descriptions as needed
};

GameException::GameException(Type type, const char* info)
	: exception(), additional_info(info)
{
	description_index = type;
}

GameException::~GameException()
{

}

const char* GameException::what(void) const throw()
{
	return Description[description_index];
}

bool GameException::operator==(GameException& other) const throw()
{
	return description_index == other.description_index;
}

GameException::operator int() const throw()
{
	return description_index;
}

std::ostream& operator<<(std::ostream& os, GameException& gameException)
{
	os << gameException.what()	<< ": " << gameException.additional_info.c_str();

	return os;
}

int GameException::testUnit(void) {

	try {
		throw GameException(GameException::Type::GENERIC_EXCEPTION, "Test");

	} catch(GameException& ex) {
		std::cout << ex << std::endl;

		std::cout << ex.what() << std::endl;

		GameException* compare = new GameException(GameException::Type::GENERIC_EXCEPTION);
		std::cout << bool(*compare == ex) << std::endl;

		std::cout << bool(int(ex) == GameException::Type::GENERIC_EXCEPTION) << std::endl;
	}
	 
	return 0;
}