#ifndef GAME_EXCEPTION_H
#define GAME_EXCEPTION_H

#include <iostream>
#include <String>
#include <exception>

class GameException : public std::exception
{
public:
	enum Type
	{
		GENERIC_EXCEPTION,
		SDL_FAIL_INIT,
		SDL_UPSTREAM_SYSTEM,
		FILE_NOT_FOUND,
		INTERNAL,
		SDL_FAIL_LOCK_SURFACE,
		SDL_OUTSIDE_DISPLAY,

		// Add more execeptions as needed
	};

	GameException(Type type, const char* info = "") throw();
	~GameException() throw();

	// Returns the Description of the error using the code given
	// in the constructor
	const char* what(void) const throw();

	bool operator==(GameException& other) const throw();

	operator int() const throw();

	// For unit tests
	static int testUnit(void);

	friend std::ostream& operator<<(std::ostream& os, GameException& gameException);

private:
	static const char *Description[];

	Type description_index;
	const std::string additional_info;

};


#endif