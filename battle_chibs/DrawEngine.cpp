#include "DrawEngine.h"
#include "GameException.h"

static void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
template <typename T> T MAX(T var1, T var2);
template <typename T> T MIN(T var1, T var2);

DrawEngine::DrawEngine(int width, int height, int bpp)
	: AbstractEngine()
{
	// Initialize video
	// set the video mode
	// check if video has already been started
	// do error checking
	int status;

	status = SDL_WasInit(SDL_INIT_VIDEO);

	// If it wasn't already initialized
	if (status == 0) {
		status = SDL_InitSubSystem(SDL_INIT_VIDEO);
		if (status == -1)
			throw GameException(GameException::Type::SDL_FAIL_INIT, SDL_GetError());
	}

	screen = SDL_GetVideoSurface();

	// if video mode wasn't already set
	if (screen == 0) {
		screen = SDL_SetVideoMode(width, height, bpp, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_ANYFORMAT);
		if (screen == 0) {
			throw GameException(GameException::Type::SDL_FAIL_INIT, "Couldn't Set Video Mode");
		}
	}

	index = 0;

}

DrawEngine::~DrawEngine()
{
	// shutdown video subsystem.
	SDL_QuitSubSystem(SDL_INIT_VIDEO);

	// delete all the SDL_Surfaces we made
	if (!imageBucket.empty()) {
		std::map<int, SDL_Surface*>::iterator iter;

		for (iter = imageBucket.begin(); iter != imageBucket.end(); ++iter) {
			SDL_FreeSurface(iter->second);
		}
	}
}

int DrawEngine::getScreenWidth()
{
	return screen->w;
}

int DrawEngine::getScreenHeight()
{
	return screen->h;
}

void DrawEngine::updateDisplay()
{
	SDL_Flip(screen);
}

int DrawEngine::createImageEntry(const char* file_name)
{
	// make new SDL_Surface
	SDL_Surface* image = IMG_Load(file_name);
	int id = index++;

	if (image == 0)
		throw GameException(GameException::Type::FILE_NOT_FOUND, SDL_GetError());

	imageBucket[id] = image;

	return id;
}

void DrawEngine::deleteImageEntry(int entry_ID)
{
	SDL_FreeSurface(imageBucket.at(entry_ID));
	imageBucket.erase(entry_ID);
}

// Taking a Rect here since I'll need to know the pos
// as well as the size of the thing I'm blitting.
// for sprite sheet support.
void DrawEngine::drawEntry(int entry_ID, Rect* srect, int dst_x, int dst_y)
{
	Rect dstrect = {0};

	dstrect.x = dst_x;
	dstrect.y = dst_y;

	// This will probably never happen, but it's happened once
	// so just in case.
	try {
		SDL_BlitSurface(imageBucket.at(entry_ID), srect, screen, &dstrect);
	} catch (std::out_of_range& ex) {
		throw GameException(GameException::Type::INTERNAL, ex.what());
	}

	// Update screen
	SDL_Flip(screen);
}

void DrawEngine::drawPixel(const RGB* color, const Vector* pos, bool update_screen)
{
	int status;
	int tmp_color;

	if ( !( (-1 < pos->x &&  pos->x < (screen->w - 1)) || (-1 < pos->y && pos->y < (screen->h - 1)) ) ) {
		throw GameException(GameException::Type::SDL_OUTSIDE_DISPLAY);
	}

	tmp_color = SDL_MapRGB(screen->format, color->red, color->green, color->blue);

	if (SDL_MUSTLOCK(screen)) {
		status = SDL_LockSurface(screen);
		if (status < 0) {
			throw GameException(GameException::Type::SDL_FAIL_LOCK_SURFACE, SDL_GetError());
		}
	}

	putpixel(screen, (int) pos->x, (int) pos->y, tmp_color);

	if (SDL_MUSTLOCK(screen)) {
		SDL_UnlockSurface(screen);
	}

	if (update_screen) {
		SDL_UpdateRect(screen, (int) pos->x,  (int) pos->y, 1, 1);
	}
}

void DrawEngine::drawPixel(int red, int green, int blue, int xpos, int ypos, bool update_screen)
{
	RGB color = { red, green, blue };
	Vector pos = { xpos, ypos };

	drawPixel(&color, &pos, update_screen);
}

void DrawEngine::drawLine(const RGB* color, const Vector* pos1, const Vector* pos2, bool update_screen)
{
	double slope = (pos2->y - pos1->y) / (pos2->x - pos1->x);
	Vector point = {0};

	// For handling the possibility that
	// pos2->x is actually smaller than pos1->x
	int start = (int) MIN(pos1->x, pos2->x);
	int end = (int) MAX(pos1->x, pos2->x);

	for (; start != end; start++) {
		point.x = start;
		// y    =  m   *  x   +   b
		point.y = slope*start + pos2->y;

		drawPixel(color, &point, false);
	}

	if (update_screen) {
		SDL_Flip(screen);
	}
}

void DrawEngine::drawLine(int red, int green, int blue, int from_x, int from_y, int to_x, int to_y, bool update_screen)
{
	RGB color = { red, green, blue };
	Vector from_pos = { from_x, from_y };
	Vector to_pos = { to_x, to_y };

	drawLine(&color, &from_pos, &to_pos, update_screen);
}

void DrawEngine::fillRect(const RGB* color, const Rect* area, bool update_screen)
{
	int tmp_color = SDL_MapRGB(screen->format, color->red, color->green, color->blue);

	SDL_FillRect(screen, (SDL_Rect*) area, tmp_color);

	if (update_screen)
		updateDisplay();
}

void DrawEngine::fillscreen(const RGB* color, bool update_screen)
{
	fillRect(color, 0, update_screen);
}

/* www.libsdl.org
 * Set the pixel at (x, y) to the given value
 * NOTE: The surface must be locked before calling this!
 */
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16 *)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        } else {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)p = pixel;
        break;
    }
}

template <typename T>
T MAX(T var1, T var2)
{
	return (var1 > var2 ? var1 : var2);
}

template <typename T>
T MIN(T var1, T var2)
{
	return (var1 < var2 ? var1 : var2);
}