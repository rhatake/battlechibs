#include "FPSRegulator.h"
#include <cmath>

FPSRegulator::FPSRegulator(double desiredFrameRate, double permitted_error)
	: timer(), secondUnits(1000), error(permitted_error), desired(desiredFrameRate)
{
	frameCount = 1;
	increment = 1;
	step = 1;
	lastFPS = 1;
	startTime = timer.getTimeMilliseconds();
}

double FPSRegulator::FPSUpdate()
{
	// This may seem obscure at first sight.
	// but the idea is, if frameCount goes negative
	// it overflowed. Thus we reset it.
	if (anyOverflows())
		reset();

	lastFPS = frameCount / ( (timer.getTimeMilliseconds() - startTime) / secondUnits );

	if (lastFPS < desired) {
		// sleep for less time
		if (step != 0) {
			step -= increment;
		}
	} else {
		// sleep for more time
		step += increment;
	}

	frameCount++;
	timer.sleep(step);
	
	return lastFPS;
}

double FPSRegulator::getFPS(bool recalc)
{
	if (recalc)
		return frameCount / ( (timer.getTimeMilliseconds() - startTime) / secondUnits );
	else
		return lastFPS;
}

void FPSRegulator::reset()
{
	frameCount = 0;
	startTime = timer.getTimeMilliseconds();
}

bool FPSRegulator::anyOverflows()
{
	// I did have more here at one point.
	// but I may just merge it at some point
	// with FPSUpdate()
	if (frameCount < 0)
		return true;
	else
		return false;
}

bool FPSRegulator::stable()
{
	if (std::abs(lastFPS - desired) < error)
		return true;
	else 
		return false;
}

void FPSRegulator::stabilize(int timeout)
{
	increment = (int) error;
	int stabilize_begin = timer.getTimeMilliseconds();

	while (!stable()) {
		if ((timer.getTimeMilliseconds() - stabilize_begin) < timeout) {
			break;
		}
		FPSUpdate();
	}

	increment = 1;
}