#include "Animation.h"
#include "GameException.h"

Animation::Animation(const char* file_name)
{
	drawEngine = DrawEngine::getInstance();

	currentState = 0;
	stateHasChanged = false;

	image_entry = drawEngine->createImageEntry(file_name);
}

Animation::~Animation()
{
	std::map<int, std::vector<DrawEngine::Rect*> >::iterator iter;
	std::vector<DrawEngine::Rect*>::iterator element;

	for (iter = states.begin(); iter != states.end(); ++iter) {
		for (element = iter->second.begin(); element != iter->second.end(); ++element) {
			delete *element;
		}
	}
}

bool Animation::stateAvailable(int state)
{
	std::map<int, std::vector<DrawEngine::Rect*> >::iterator iter;
	iter = states.find(state);

	// if iter reaches end(), then it doesn't have a key of state
	// and is available
	if (iter == states.end())
		return true;
	else
		return false;
}

bool Animation::addState(int requested_state)
{
	if ( !(stateAvailable(requested_state)) ) {
		return false;
	}

	// if it's not there add it
	states.insert( std::make_pair(requested_state, std::vector<DrawEngine::Rect*>()) );

	// return true if the state was added successfully
	return true;
}

void Animation::addFrameToState(int state, const DrawEngine::Rect* area)
{
	DrawEngine::Rect* tmp = new DrawEngine::Rect(*area);

	// if the state is available, then it hasn't been used
	// and hence shouldn't be referenced.
	if ( stateAvailable(state) ) {
		throw GameException(GameException::Type::INTERNAL, "Animation::addFrameToState(): Tried to Reference Unused State");
	}

	states[state].push_back(tmp);
}

void Animation::removeState(int state)
{
	std::vector<DrawEngine::Rect*>::iterator iter;

	for (iter = states[state].begin(); iter != states[state].end(); iter++) {
		delete *iter;
	}

	states.erase(state);
}

void Animation::changeState(int state_ID)
{
	currentState = state_ID;
	stateHasChanged = true;
}

bool Animation::empty()
{
	return states.empty();
}

void Animation::draw(int x, int y)
{
	if (states.empty())
		throw GameException(GameException::Type::INTERNAL, "Animation::draw(), Tried to Draw While State List Empty");

	// if the state is available, then it hasn't been used
	// and hence shouldn't be referenced.
	if ( stateAvailable(currentState) ) {
		throw GameException(GameException::Type::INTERNAL, "Animation::draw(): Tried to Reference Unused State");
	}

	// If either the state has changed, or we need to repeat the animation
	// go back to the beginning
	if (stateHasChanged || index == states[currentState].end() ) {
		index = states[currentState].begin();
		stateHasChanged = false;
	}

	drawEngine->drawEntry(image_entry, *index, x, y);

	index++;
}

#include "Timer.h"
#include "InputEngine.h"

void Animation::runTest()
{
	Animation::Rect selector;

	selector.w = 47;
	selector.h = 88;

	Animation animator("..\\Resources\\SNES - ANIMATION TEST DBZ.png");
	int SUPER_SAYIAN = 1;

	selector.x = 0;
	selector.y = 0;


	animator.addState(SUPER_SAYIAN);

	for (int i = 0; i != 3; i++) {
		animator.addFrameToState(SUPER_SAYIAN, &selector);
		selector.x += 47;
	}
		
	Timer timer;
	DrawEngine* drawEngine = DrawEngine::getInstance();
	InputEngine* input = InputEngine::getInstance();
	DrawEngine::RGB color = { 0xff, 0xff, 0xff };

	animator.changeState(SUPER_SAYIAN);

	char c = ' ';
	while (c != 'q') {
		c = input->getInput();

		drawEngine->fillscreen(&color);
		animator.draw(320, 240);
		timer.sleep(200);
	}
}