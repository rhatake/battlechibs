#include "SDL.h"

#include "Game.h"

#include "GameException.h"

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;
 
// NOTE! - Change the subsytem back to WINDOWS under Linker|System to test the draw engine!
// Note - You may be able to ignore that first note.

int main(int argc, char** argv)
{
	try {
		int status;

		cout << "Initializing SDL" << endl;

		// 0, only initialize SDL
		status = SDL_Init(0);

		if ( status == -1)
			throw GameException(GameException::Type::SDL_FAIL_INIT, SDL_GetError());

		cout << "SDL Initialized" << endl;

		// Run Game
		Game game = Game();
		game.run();
		

	} catch (GameException& ex) {
		cerr << ex << endl;
	} catch (std::exception& ex) {
		cerr << "General Exception: " << ex.what();
	} catch (...) {
		cerr << "An Unexpected Error Occured" << endl;
	}

	cout << "Shutting Down Subsystems" << endl;
	SDL_Quit();

	cout << "Exiting...\n";
	 
	return 0;
}