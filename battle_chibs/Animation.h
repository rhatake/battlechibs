#ifndef ANIMATION_H
#define ANIMATION_H

#include "DrawEngine.h"
#include <map>
#include <vector>

class Animation
{
public:
	typedef DrawEngine::Rect Rect;

	Animation(const char* file_name);
	~Animation();

	// returns true if state was added, false otherwise
	bool addState(int requested_state);

	void addFrameToState(int state, const Rect*);

	// check if there are no states
	bool empty();

	void removeState(int state);

	// change the current state
	void changeState(int state_ID);

	void draw(int x, int y);

	// for testing
	static void runTest(void);
	

private:
	DrawEngine* drawEngine;

	std::map<int, std::vector<DrawEngine::Rect*> > states;
	std::vector<DrawEngine::Rect*>::iterator index;

	int currentState;
	bool stateHasChanged;
	int image_entry;

	// check if state has been added
	bool stateAvailable(int state);

};

#endif