#include "Game.h"

#include <iostream>

Game::Game()
{
	// Initialize all the engines here
	gameSpeedRegulator = new FPSRegulator();

	// Some subsystems depend on drawEngine
	// having to start it first does implicitly
	// give away a bit of the implementation
	drawEngine = DrawEngine::getInstance();

	input = InputEngine::getInstance();
}

Game::~Game()
{
	delete gameSpeedRegulator;
}

void Game::run()
{
	char c = ' ';
	
	gameSpeedRegulator->stabilize();

	while (c != 'q') {
		if (!(c = input->getInput())) {
			update();
			// Continue so we don't end still trying to process
			// the keypress.
			continue;
		}

		// Handle keypress here
		std::cout << "Keypress: " << c << "\r";
	}
}

void Game::update()
{
	// Put idle code here
	idleUpdate();
	gameSpeedRegulator->FPSUpdate();
}

void Game::idleUpdate()
{
	std::cout << "FPS: " << gameSpeedRegulator->getFPS() << "\r";
}