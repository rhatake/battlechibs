#ifndef GAME_H
#define GAME_H

#include "DrawEngine.h"
#include "GameException.h"
#include "InputEngine.h"
#include "FPSRegulator.h"


class Game
{
public:
	Game();
	~Game();

	void run(void);

private:
	InputEngine* input;
	FPSRegulator* gameSpeedRegulator;
	DrawEngine* drawEngine;

	// Principle update that takes care of game status monitoring
	// and delegating idle tasks to other methods.
	void update(void);

	// Place to put code to be excuted in idle state that has
	// to do with game mechanics only
	void idleUpdate(void);
};

#endif