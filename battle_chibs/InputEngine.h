#ifndef INPUT_ENGINE_H
#define INPUT_ENGINE_H

#include "AbstractEngine.h"

#include "SDL.h"

class InputEngine : public AbstractEngine<InputEngine>
{
public:
	// returns character or 0 if no character is available
	char getInput(void);

	// For unit tests
	static void testUnit(void);

private:
	// We're a singleton now; don't need these.
	friend AbstractEngine;
	InputEngine();
	InputEngine(const InputEngine&);
	InputEngine& operator=(const InputEngine&);

	// For collecting keyboard events
	SDL_Event event;
};

#endif